var original_vp_height = $(window).height();

// actions to execute when DOM loads
$(document).ready(function () {
    $(window).on('load', adjust_elements($(window).height()));
});

//------------------------------------------------------------//

// what to do on window resize event
$(window).resize(function () {
    var viewportHeight = $(window).height();
    adjust_elements(viewportHeight);
});

//------------------------------------------------------------//

// adjusts the position attributes (bottom etc') of a few elements
function adjust_elements(viewportHeight) {      // stretching container div to bottom of page
    var bottom;
    if (original_vp_height > viewportHeight)    bottom = -(original_vp_height - viewportHeight);
    else    bottom = 0;
    $('footer').css('bottom', bottom);
    $("#main_container").css('bottom', bottom);
};

//------------------------------------------------------------//