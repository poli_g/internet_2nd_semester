var user_timer;         // to save timer of user list fetching
var post_timer;         // to save timer of posts fetching
var curr_user_data;     // to save data of currently logged in user
get_curr_user_data();

$(document).ready(function () {
    get_online_users();
    get_posts();
    user_timer = setInterval(function () { get_online_users(); }, 10000);
    post_timer = setInterval(function () { get_posts(); }, 10000);
});

function get_curr_user_data() {
    $.ajax({
        type: "GET",
        url: "homepage/get_curr_user_data",
        dataType: 'json',
        error: function () { alert('Error getting current user'); },
        success: function (result) {
            curr_user_data = result;
        }
    });
}

/* To handle online user list: */

// retrieves list of online users using ajax call
function get_online_users() {
    $.ajax({
        type: "GET",
        url: "homepage/get_online_users",
        dataType: 'json',
        error: function (request, status, thrownError) { $('#online_users').html("Error getting online users!\n\n" + "Status: " + request.status + "\n\n" + "Response text: " + request.responseText + "\n\n" + "Thrown error: " + thrownError); },
        success: function (result) {
            clear_online_users();
            $.each(result, function (i, user) {
                $('#online_users ul').append('<li id="'+ user.id +'" onclick="filter_feed_by_user(this.id)">' + user.username + '</li>');
            });
        }
    });
}

// filters feed according to clicked username in users list
function filter_feed_by_user(id) {
    stop_post_timer();  // we don't want the feed to revert

    $('#feed li').each(function (i, li) {
        if ($(li).find('span.username').attr('id') != id)
            li.remove();
    });
}

// clears online user list
function clear_online_users() {
    $('#online_users ul').empty();
}

// allows filtering
function filter_online_users(filter) {
    // stopping timer:
    clearInterval(user_timer);
    $('#online_users ul li').each(function (id, li) {
        if ($(li).html().indexOf($(filter).val()) != 0)
            $(li).css('visibility', 'hidden');
        else
            $(li).css('visibility', 'visible');
    });
}

// renew timer fo user list fetching
function renew_user_timer() {
    user_timer = setInterval(function () { get_online_users(); }, 10000);
}

/* Posts */

function check_post_submission(post) {
    if (check_enter($(post).val()))
        $('#' + $(post).parent().attr('id')).submit();
}

function check_comment_submission(comment) {
    if (check_enter($(comment).val())) {
        add_new_comment($(comment).val().trim(), $(comment).parent().parent().attr('id'));
        $(comment).val('');
    }
}

// checks is enter was pressed
function check_enter(txt) {
    if (txt.substr(txt.length - 1) == '\n')   // enter was pressed
        return true;
    return false;
}

// adds a new comment to 
function add_new_comment(comment_str, post_id) {
    $.ajax({
        type: "POST",
        url: "homepage/add_comment_to_post",
        async: false,     // making the request synchronized
        data: {
            post_id: post_id,
            user_id: curr_user_data.id,
            comment: comment_str
        },
        error: function (request, status, thrownError) { $('html').replace("Error adding comment!\n\n" + "Status: " + request.status + "\n\n" + "Response text: " + request.responseText + "\n\n" + "Thrown error: " + thrownError); },
        success: function (comment) {
            /*
            if (!$('#' + post_id).hasClass('no_comments')) {
                    clear_all_comments_for_post(post_id);
                }
                get_post_comments($('#' + post_id).find('.show_comments'));*/
            }
    });
}

function clear_all_comments_for_post(post_id) {
    $('#' + post_id + ' ul li').remove();
}

function get_posts() {
    $.ajax({
        type: "GET",
        url: "homepage/get_posts",
        dataType: 'json',
        error: function (request, status, thrownError) {
            $('html').replace("Error Getting posts!\n\n" + "Status: " + request.status + "\n\n" + "Response text: " + request.responseText + "\n\n" + "Thrown error: " + thrownError);
        },
        success: function (result) {
            clear_posts();
            $.each(result, function (i, post) {
                add_post(post);
            });
        }
    });
}

// add a post
function add_post(post) {
    var post_time = new Date(1000 * parseInt(post.timestamp));    // adding 3 hrs for local time and converting to miliseconds
    var post_str = '<li id="' + post.id + '" class="no_comments">' + '<span class="username bold" id="' + post.owner_id + '">' + post.username + '</span>' +
        '<span class="timestamp bold">' + post_time.toDateString() + '</span>' + '<p>' + post.post + '</p>' +
        '<form id="add_comment" method="post" action="homepage/add_comment_to_post">' +
        '<textarea name="comment" placeholder="Comment on this" onkeyup="check_comment_submission(this)" onfocus="stop_post_timer()" onblur="renew_posts_timer()"></textarea>' +
        '<input name="post_id" type="hidden" value="' + post.id + '"></input>' +
        '<input type="submit">' +
        '</form>' +
        '<ul class="comments"></ul>' +
        '<button class="show_comments" onclick="get_post_comments(this)">Show Comments</button>';
    
    if (curr_user_data.id == post.owner_id)
        post_str +='<button class="delete_post" onclick="delete_post(this)">Delete this post</button>';
    post_str += '</li>';
    
    $('#feed').append(post_str);
}

// stops the posts fetching timer
function stop_post_timer() {
    clearInterval(post_timer);
}

// renew the posts fetching timer
function renew_posts_timer() {
    post_timer = setInterval(function () { get_posts(); }, 10000);
}

// removes all posts
function clear_posts() {
    $('#feed').empty();
}

// delete a post
function delete_post(post) {
    $.ajax({
        type: "POST",
        url: "homepage/delete_post",
        async: false,     // making the request synchronized
        data: {
            post_id: $(post).parent().attr('id')
        },
        error: function (request, status, thrownError) { $('html').replace("Error deleting post!\n\n" + "Status: " + request.status + "\n\n" + "Response text: " + request.responseText + "\n\n" + "Thrown error: " + thrownError); },
        success: function () { $('#'+$(post).parent().attr('id')).remove(); }
    });
}

// loads post's comments:
function get_post_comments(post) {
    // stopping timer
    stop_post_timer();

    $.ajax({
        type: "POST",
        url: "homepage/retrieve_post_comments",
        async: false,     // making the request synchronized
        dataType: 'json',
        data: {
            post_id: $(post).parent().attr('id')
        },
        error: function (request, status, thrownError) { alert("Error getting comments!\n\n" + "Status: " + request.status + "\n\n" + "Response text: " + request.responseText + "\n\n" + "Thrown error: " + thrownError); },
        success: function (comments) {
            if (comments != null) {
                var post_id = $(post).parent().attr('id');
                $.each(comments, function (i, comment) {
                    append_comment(post_id, comment);
                });
                // removing "show comments" button:
                post.remove();
                // adding "hide comments" button:
                $('#' + post_id).append('<button class="remove_comments" onclick="remove_comments(this)">Close comments</button>');
                // changing post class to "yes_comments" so we know they're open
                $('#' + post_id).addClass('yes_comments').removeClass('no_comments');
            }
        }
    });
}

// add new comment to html
function append_comment(post_id, comment) {
    var timestamp = new Date(1000 * parseInt(comment.timestamp));

    var comment_html = '<li id="' + comment.id + '">' + '<span class="username bold">' + comment.username + '</span>' +
        '<span class="timestamp bold">' + timestamp.toDateString() + '</span>' + '<p>' + comment.comment + '</p>';
    if (curr_user_data.id == comment.owner_id)
        comment_html += '<button class="delete_comment" onclick="delete_comment(this)">Delete this comment</button>';
    comment_html += '</li>';

    $('#' + post_id + ' ul').append(comment_html);
}

// removes all comments from a given post
function remove_comments(post) {
    console.log(post);
    $('#' + $(post).parent().attr('id') + ' ul li').remove();  // removing all comments
    $('#' + $(post).parent().attr('id')).append('<button class="show_comments" onclick="get_post_comments(this)">Show Comments</button>');
    post.remove();  // removing "hide comments" button
    $('#' + $(post).parent().attr('id')).addClass('no_comments').removeClass('yes_comments');

    // renewing timer
    renew_posts_timer();
}

function delete_comment(comment) {
    $.ajax({
        type: "POST",
        url: "homepage/delete_comment",
        async: false,     // making the request synchronized
        data: {
            comment_id: $(comment).parent().attr('id')
        },
        error: function (request, status, thrownError) { $('html').replace("Error deleting comment!\n\n" + "Status: " + request.status + "\n\n" + "Response text: " + request.responseText + "\n\n" + "Thrown error: " + thrownError); },
        success: function () {
            console.log($($(comment).parent().parent().parent().find('ul').find('li')));
            // checking if this is was the last comment:
            if ($('#' + $(comment).parent().parent().parent().find('ul').attr('id')).find('li').length == 0)
                remove_comments($(comment).parent().parent().parent().find('.remove_comments')[0]);
            else
                $(comment).parent().remove();
        }
    });
}