var email_ok = false;    // global for preventing form submission if email is taken
var nickname_ok = false; // global for preventing form submission if nickname is taken

function verify_registration() {
    var error = false;
    var min_pass_length = 8;

    var name = $('#new_name');
    var username = $('#new_username');
    var email = $('#new_email');
    var pass = $('#new_pass');
    var pass_again = $('#new_pass_verification');

    // verifying both first and last name were provided
    if (!name.find('input').val().trim() || name.find('input').val().trim().indexOf(' ') < 0) {
        add_error_to_field(name, 'Please enter full name');
        error = true;
    } else
        remove_error_to_field(name);

    if (!username.find('input').val().trim() || username.find('input').val().trim().indexOf(' ') > 0) {
        add_error_to_field(username, 'Please enter username, no spaces allowed');
        error = true;
    } else
        remove_error_to_field(username);

    if (!email.find('input').val().trim()) {
        add_error_to_field(email, 'Please enter valid Email');
        error = true;
    } else
        remove_error_to_field(email);

    // validating password:
    var error_msg;

    if (!pass.find('input').val() || pass.find('input').val().length < min_pass_length) {
        error_msg = 'Password must be at least 8 characters long';
    }
    // checking the pass contains both alphanumeric and numbers:
    var contains_numbers = false;
        
    var i = pass.find('input').val().length;
    while (i--) {
        if ($.isNumeric(pass.find('input').val()[i]))
            contains_numbers = true;
    }

    if (!contains_numbers)
        if (!error_msg)
            error_msg = 'Password must contain numbers';
        else
            error_msg += ' and contain numbers';

    if (error_msg) {
        add_error_to_field(pass, error_msg);
        error = true;
    }
    else
        remove_error_to_field(pass);

    if (pass_again.find('input').val() != pass.find('input').val()) {
        add_error_to_field(pass_again, 'The passwords do not match');
        error = true;
    } else
        remove_error_to_field(pass_again);

    if (!error && email_ok && nickname_ok) return true;
    else return false;
}

function add_error_to_field(field, msg) {
    field.find('span').html(msg);
    field.find('input').addClass('highlight');
}

function remove_error_to_field(field) {
    field.find('span').html('');
    field.find('input').removeClass('highlight');
}

function check_email_existance(email) {
    $.ajax({
        type: "POST",
        url: "registration/validate_credentials",
        async: false,     // making the request synchronized
        dataType: "json",
        data: {
            type: 'email',
            email: $(email).val().trim()
        },
        error: function () { alert('An Ajax error has occured'); },
        success: function (result) {
            if (!result.email) {
                add_error_to_field($(email).parent(), 'Email already registered, <a href="#">forgot password?</a>');
                email_ok = false;
            } else {
                email_ok = true;
                remove_error_to_field($(email).parent());
            }
        }
    });
}

function check_username_existance(username) {
    $.ajax({
        type: "POST",
        url: "registration/validate_credentials",
        async: false,     // making the request synchronized
        dataType: "json",
        data: {
            type: 'username',
            username: $(username).val().trim()
        },
        error: function () { alert('An Ajax error has occured'); },
        success: function (result) {
            if (!result.username) {
                add_error_to_field($(username).parent(), 'Username taken');
                nickname_ok = false;
            } else {
                nickname_ok = true;
                remove_error_to_field($(username).parent());
            }
        }
    });
}

