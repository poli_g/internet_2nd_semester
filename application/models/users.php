<?php

/**
 * users is a Model designed to handle all DB queries related to the users of the system.
 * wether it is registration validation, user login, password retrieval, etc'.
 * 
 * @version 1.0
 * @author Polina
 */
class Users extends CI_Model {
    
    // checking existance of username\email in our DB, we don't allow multiple regirstrations with same username and\or email
    function check_existance($type, $val) {
        $query = $this->db->query("select id from users where ".$type."= ? ", $val);
        if (!$query)
            return false;
        else if ($query->num_rows() > 0)    // value was found in DB
            return true;
        else
            return false;
    }
    
    function add_user ($user_data) {
        $this->db->query('insert into users (email, username, first_name, last_name, password_md5) values (?, ?, ?, ?, ?)', 
                         array('email' => strtolower($user_data['email']), 
                               'username' => strtolower($user_data['username']), 
                               'first_name' => strtolower($user_data['first_name']), 
                               'last_name' => strtolower($user_data['last_name']), 
                               'pass' => $user_data['pass']));
    }
    
    // logs user in by adding a session for their user id in DB
    function open_session($email, $pass) {
        // retrieving user id:
        $login = $this->db->query('select id from users where email=? and password_md5=?', array($email, $pass));
        if (!$login || $login->num_rows() == 0)
            return false;
        $id = $login->row()->id;
        
        // preparing session id:
        $session = md5($email . $id);
        
        // checking if user already logged in, if so, login will be allowed on current session and old session will be closed.
        $result = $this->db->query('select user_id from sessions where user_id=?', $id);
        if ($result->num_rows() > 0)    // user already logged in
            $this->db->query('update sessions set session = ? where user_id = ?', array($session, $id));
        else
            $this->db->query('insert into sessions (user_id, session) values (?, ?)', array($id, $session));
        
        return $session;
    }
    
    // logs user out in DB
    function close_session ($email) {
        $id = $this->db->query('select id from users where email=?', $email)->row()->id;
        
        $result = $this->db->query('delete from sessions where user_id=?', $id);
    }
    
    // verifies the user session matches the one logged for them in the DB
    function check_login($user_id, $user_session) {
        $result = $this->db->query('select session from sessions where user_id=?', $user_id);
        
        if ($result && $result->num_rows() > 0)
            if ($result->row()->session == $user_session)
                return true;
            
        return false;
    }
    
    // changes user password to given one
    function set_default_pass($email, $pass) {
        $this->db->query('update users set password_md5 = ? where email = ?', array($pass, $email));
    }
    
    // returns an array of names of all online users
    function get_online_users() {
        
        $result = $this->db->query('select u.id, u.username from users as u inner join sessions as s on s.user_id=u.id where 1=1');
       
        return $result->result_array();
    }
    
    // returns the current user credentials
    function get_curr_user_data($type, $val) {
        if ($type !== 'id' && $type !== 'username' && $type !== 'email' && $val->length != 0)    // only three unique values per user
            return false;  
        
        $result = $this->db->query('select * from users where '.$type.' = \''.$val.'\'');
        if ($result && $result->num_rows() > 0)
            return $result->first_row('array');
        return false;
    }
}
