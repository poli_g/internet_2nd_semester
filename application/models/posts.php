<?php

/**
 * Posts is a Model designed to handle all DB queries related to the posts of all registered users.
 * 
 * @version 1.0
 * @author Polina
 */
class Posts extends CI_Model {
    
    // retrieves posts
    function get_posts() {
        $result = $this->db->query('select p.id, p.owner_id, p.timestamp, p.post, u.username from posts p inner join users u on u.id = p.owner_id');
        
        return $result->result_array();
    }
    
    // adds new post to DB. on success returns post's id, on failure false.
    function add_post($txt, $creator_id) {
        $timestamp = time();
        
        $result = $this->db->query('insert into posts (owner_id, timestamp, post) values (?, ?, ?)', array($creator_id, $timestamp, $txt));
        if ($result) {
            $insert_id = $this->db->insert_id();    // getting id of last inserted post
            return $insert_id;
        }
        return false;
    }
    
    // deletes post with given is and all of it's comments from db
    function delete_post($id) {
        $this->db->query('delete from posts where id=?', $id);
        $this->db->query('delete from comments where post_id=?', $id);
    }
    
    // deletes comment from DB
    function delete_comment($id) {
        $this->db->query('delete from comments where id=?', $id);
    }
    
    // gets all comments of given post id from db
    function retrieve_post_comments($id) {
        $comments = $this->db->query('select c.id, c.post_id, c.owner_id, c.timestamp, c.comment, u.username from comments c inner join users u on u.id = c.owner_id', $id);
        
        if ($comments && $comments->num_rows() > 0)
            return $comments->result_array();
    }
    
    // adds a comment to specific post
    function add_comment_to_post($post_id, $comment_str, $owner_id) {
        $timestamp = time();
        
        $result = $this->db->query('insert into comments (post_id, owner_id, timestamp, comment) values (?, ?, ?, ?)', array($post_id, $owner_id, $timestamp, $comment_str));
        if ($result) {
            $id = $this->db->insert_id();
            $comment = $this->db->query('select c.id, c.post_id, c.owner_id, c.timestamp, c.comment, u.username from comments c inner join users u on u.id = c.owner_id where c.id=?', $id);
            if ($comment && $comment->num_rows() > 0)
                return $comment->result_array(0);
            else
                return false;
        }
        return false;
    }
}
