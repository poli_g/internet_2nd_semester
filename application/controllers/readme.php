<?php

/**
 * Class1 short summary.
 *
 * Class1 description.
 *
 * @version 1.0
 * @author Polina
 */
class Readme extends CI_Controller{
    function index() {
        $data = array();
        
        $data['general'] = false;
        $data['menu'] = $this->load->view('general/menu', NULL, true);
        $data['footer'] = $this->load->view('general/footer', NULL, true);
        $data['general_content'] = $this->load->view('readme', NULL, true);
        $this->load->view('general/layout', $data);
    }
}
