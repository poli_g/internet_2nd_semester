<?php

/**
 * homepage is the controller for the, well, Home Page...
 * The homepage is where all the posts are displayed after you've logged in. 
 * If you're not logged in you will be redirected to the login page.
 *
 * @version 1.0
 * @author Polina
 */
class Homepage extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('posts');
    }
    
    function index() {
        // checking if user logged in, if not, redirect to login page:
        $user_data = $this->users->get_curr_user_data('email', $this->session->userdata('email'));
        if (!$this->users->check_login($user_data['id'], $this->session->userdata('session'))) {
            redirect(base_url().'index.php/login');
        }
        
        $data = array();
        
        $data['general'] = false;
        $data['menu'] = $this->load->view('general/menu', NULL, true);
        $data['footer'] = $this->load->view('general/footer', NULL, true);
        $data['general_content'] = $this->load->view('user_list', NULL, true) . $this->load->view('posts', NULL, true);
        $this->load->view('general/layout', $data);
    }
    
    function get_curr_user_data() {
        echo json_encode($this->users->get_curr_user_data('email', $this->session->userdata('email')));
    }
    
    function get_online_users() {
        echo json_encode($this->users->get_online_users());
    }
    
    function get_posts() {
        echo json_encode($this->posts->get_posts());
    }
    
    function add_post() {
        $post_txt = $this->input->post('new_post');     // retrieving post
        // retrieving user credentials:
        $user_data = $this->users->get_curr_user_data('email', $this->session->userdata('email'));
        
        if ($user_data !== false) {
            $user_id = $user_data['id'];
            
            // adding post to DB:
            $insert = $this->posts->add_post($post_txt, $user_id);
            if ($insert === false)
                ;//display error?
        }
        redirect('homepage');
    }
    
    function delete_post() {
        $this->posts->delete_post($this->input->post('post_id'));
    }
    
    function retrieve_post_comments() {
        $comments = $this->posts->retrieve_post_comments($this->input->post('post_id'));
        if ($comments !== false)
            echo json_encode($comments);
        echo false;
    }
    
    function add_comment_to_post() {
        $post_id = $this->input->post('post_id');
        $comment_str = $this->input->post('comment');
        $publisher_id = $this->input->post('user_id');
        
        echo json_encode($this->posts->add_comment_to_post($post_id, $comment_str, $publisher_id));
    }
    
    function delete_comment() {
        $this->posts->delete_comment($this->input->post('comment_id'));
    }
}
