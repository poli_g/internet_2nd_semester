<?php

/**
 *
 * @version 1.0
 * @author Polina
 */
class Registration extends CI_Controller {
    
    function index() {
        $data = array();
        
        $data['general'] = true;
        $data['menu'] = $this->load->view('general/menu', NULL, true);
        $data['footer'] = $this->load->view('general/footer', NULL, true);
        
        $data['main_content'] = $this->load->view('registration', NULL, true);
        $this->load->view('general/layout', $data);
    }
    
    // checks that provided info via ajax call is valid: username available and email doesn't exist in DB
    function validate_credentials() {
        // making sure we got here in a valid way
        if (!$this->input->is_ajax_request())
            show_404();
        
        $val_type = $this->input->post('type'); // email OR username can be checked
        
        echo json_encode(array($val_type => !$this->users->check_existance($val_type, $this->input->post($val_type))));
    }
    
    // registers the user in the DB
    function register_user() {
        $name = explode(' ', $this->input->post('name'), 2);
        
        $form_data = array('first_name' => $name[0],
                           'last_name' => $name[1],
                           'username' => $this->input->post('username'),
                           'email' => $this->input->post('email'),
                           'pass' => md5($this->input->post('pass')));
        
        // adding user
        $this->users->add_user($form_data);
        
        // redirecting to login page so user can login with brand new account
        $this->session->set_userdata(array('login_error' => false));
        redirect(base_url().'index.php/login');
    }
}
