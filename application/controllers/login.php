<?php

/**
 *
 * @version 1.0
 * @author Polina
 */
class Login extends CI_Controller {
    
    function index() {
        $data = array();
        
        // checking wether there is a login error:
        if ($this->session->userdata('login_error'))
            $data['error'] = $this->load->view('login_error', NULL, true);
        if (!empty($this->session->userdata('email'))) {
            $data_user = $this->users->get_curr_user_data('email', $this->session->userdata('email'));
            if ($this->users->check_login($data_user['id'], $this->session->userdata('session'))) {
                redirect('homepage');
            }
        }
            
        
        $data['general'] = true;
        $data['menu'] = $this->load->view('general/menu', NULL, true);
        $data['footer'] = $this->load->view('general/footer', NULL, true);
        $data['main_content'] = $this->load->view('login', $data, true);
        $data['secondary_content'] = $this->load->view('login_retrieval', NULL, true);
        $this->load->view('general/layout', $data);
    }
    
    // function for loggin in the user (opening a session for him in DB and storing login data in session)
    function user_login() {
        $email = $this->input->post('email');
        $pass = md5($this->input->post('pass'));
        
        $session = $this->users->open_session($email, $pass);
        if (!$session)
            $this->session->set_userdata(array('login_error' => true));
        else 
            $this->session->set_userdata(array('logged_in' => true, 'session' => $session, 'email' => $email, 'login_error' => false));
            
        redirect(base_url().'index.php/homepage');
    }
    
    function user_logout () {
        if ($this->session->userdata('logged_in')) {
            $this->users->close_session($this->session->userdata('email'));
            
            //$this->session->set_userdata(array('logged_in' => false, 'session' => '', 'email' => ''));
            
            redirect(base_url().'index.php/login');
        }
    }
    
    function password_restore() {
        // retrieving email:
        $email = $this->input->post('registration_email');
        
        if ($this->users->check_existance('email', $email)) {   
            $new_pass = substr(md5(rand()), 0, 8);  // generating new 8-characters long pass
            $this->users->set_default_pass(md5($new_pass));   // changing password in DB to default random passoword
            
            // sending new password in email
            $message = 'Hello, Your temporary password is: '.$new_pass.'. Please Change it as soon as possible.';
            mail($email, 'Password retrieval', $message);
        }
        
        // returning to login page
        redirect(base_url().'index.php/login');
    }
}
