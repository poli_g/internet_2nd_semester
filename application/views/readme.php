<div id="readme">
	<h2>poli.granot@gmail.com, ssvety100@gmail.com, daniel.mansfield@mail.huji.ac.il</h2>
	<h2>Authors: Granot Polina, Shur Svetlana, Mansfield Daniel  </h2>
	<h2>project - part 2 - A website<br>_________________________________________</h2>
	<p>
	<br>In this exersice we implemented a facebook like website, using a Codeigniter PHP framework (was authorized by Solange) for the server side, and jQuery / HTML for the client side.
	<br>At the Login page you may login with an existing user, click a link that will land you at the Registration page, or choose to restore your password in case you have lost it.
    <br>At the Homepage of the website you see a posts feed and a list of online users:
    <br><b>The Feed</b> - registered user may create a new post and/or comments on their own or other people posts.
    If a user is the creator of a post or a comment they may delete it.
	<br><b>The user List</b> - All online users are shown in the list and you may filter the list using the input field above the list. When a username is clicked the feed will be 
    filtered and will olny show posts by the selected user.
	</p>
    <br>
	<p>
        <b><i>IMPORTANT NOTE - </i></b> If you wish to test the system locally you will need to:
        <ul> 
            <li>Install a local DB (a DB dump is available at the root directory of the project)</li>
            <li>Configure the local path in the config file located at Application -> Config -> config.php</li>
            <li>Configure the database in the file located at Application -> Config -> database.php according to your local DB credentials</li>
        </ul>
	</p></div>