<script src="<?php echo base_url()."assets/js/registration_verification.js"; ?>"></script>
<form onsubmit="return verify_registration()" action="registration/register_user" method="post" id="registration_form">
    <div class="spacer"></div>
    <label id="new_name">
        <input type="text" placeholder="Full name" name="name"><span></span>
    </label>
    
    <label id="new_username">
        <input type="text" placeholder="Username" name="username" onblur="return check_username_existance(this)"><span></span>
    </label>

    <label id="new_email">
        <input type="email" placeholder="Email" name="email" onblur="return check_email_existance(this)"><span></span>
    </label>
    
    <label id="new_pass">
        <input type="password" placeholder="Select password" name="pass"><span></span>
    </label>
    
    <label id="new_pass_verification">
        <input type="password" placeholder="Retype password" name="pass_validation"><span></span>
    </label>
    
    <label>
    <input type="submit">
        </label>
    <div class="spacer"></div>
</form>