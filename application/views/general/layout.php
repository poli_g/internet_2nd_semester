<!DOCTYPE>
<html>
    <head>
        <?php echo '<style type="text/css">'.
                   '@font-face {font-family: my_minima; src: url("'.base_url().'assets/fonts/minima.ttf");}'.
                   '</style>'; ?>
        <link rel="stylesheet" href="<?php echo base_url()."assets/css/style.css"; ?>" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url()."assets/css/menu.css"; ?>" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url()."assets/css/footer.css"; ?>" type="text/css">

        <link rel="icon" type="image/png" href="<?php echo base_url()."assets/images/wheel4.png"; ?>">
        <meta name="author" content="Polina Granot poli.granot@gmail.com & Sveta Shur">
        <meta charset="utf-8">
        <script src="<?php echo base_url()."assets/js/jquery-2.0.3.min.js"; ?>"></script>
        <script src="<?php echo base_url()."assets/js/general.js"; ?>"></script>
    </head>
    <body>
        <menu>
            <?php echo $menu; ?>
        </menu>
        <main>
            <div class="main_container"  <?php if ($general){ echo 'id="main_container"';}?> style="clear:both;">
                <?php if (!empty($general_content)) echo $general_content; ?>
                <?php if ($general) {
                            echo '<div id="subcontainer1">';
                            if (!empty($main_content)) echo $main_content;
                            echo '<div id="subcontainer2">';
                            if (!empty($secondary_content)) echo $secondary_content;
                            echo '</div></div>';
                      }
                ?>
             
            </div>
            
        </main>
        <footer>
            <?php echo $footer; ?>
        </footer>
    </body>
</html>